<?php

namespace Drupal\address_autocomplete_suggestion\Element;

use Drupal\address\Element\Address;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an address_autocomplete suggestion form element.
 *
 * Usage example:
 *
 * @code
 * $form['address_autocomplete_suggestion'] = [
 *   '#type' => 'address_autocomplete_suggestion',
 * ];
 * @endcode
 *
 * @FormElement("address_autocomplete_suggestion")
 */
class AddressAutocompleteSuggestion extends Address {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();

    $info['#process'][] = [
      get_class($this),
      'processAutocomplete',
    ];

    return $info;
  }

  /**
   * {@inheritDoc}
   */
  public static function processAutocomplete(&$element, FormStateInterface $form_state, &$complete_form) {
    $element["#attached"]["library"][] = 'address_autocomplete_suggestion/address_autocomplete_suggestion';
    $element["address_line1"]['#autocomplete_route_name'] = 'address_autocomplete_suggestion.addresses';
    $element["address_line1"]["#attributes"]['placeholder'] = t('Please start typing your address...');
    return $element;
  }

}
