<?php

namespace Drupal\address_autocomplete_suggestion\Plugin\Field\FieldWidget;

use Drupal\address\Plugin\Field\FieldWidget\AddressDefaultWidget;
use Drupal\address_autocomplete_suggestion\Form\SettingsForm;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'address_autocomplete_suggestion' widget.
 *
 * @FieldWidget(
 *   id = "address_autocomplete_suggestion",
 *   label = @Translation("Address autocomplete Suggestion"),
 *   field_types = {
 *     "address"
 *   }
 * )
 */
class AddressAutocompletSuggestioneWidget extends AddressDefaultWidget {

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['address']['#type'] = 'address_autocomplete_suggestion';

    $config = $this->configFactory->get(SettingsForm::$configName);
    if (!$config->get('active_plugin')) {
      $element['message'] = [
        '#type' => 'item',
        '#markup' => t('Address autocomplete provider isn\'t selected. You can do it <a href="@url">here</a>.', [
          '@url' => Url::fromRoute('address_autocomplete_suggestion.settings')
            ->toString(),
        ]),
        '#wrapper_attributes' => [
          'class' => ['messages', 'messages--warning'],
        ],
        '#weight' => -10,
      ];
    }

    return $element;
  }

}
