(function ($, Drupal,once) {
  Drupal.behaviors.address_autocomplete_suggestion = {
    attach: function (context, settings) {
      $(once('initiate-autocomplete', 'input.address-line1', context)).each( function (){
         var form_wrapper = $(this).closest('.js-form-wrapper');
         var ui_autocomplete = $(this).data('ui-autocomplete');
         ui_autocomplete.options.select = function (event, ui) {
           event.preventDefault();
           form_wrapper.find('input.address-line1').val(ui.item.street_name);
           form_wrapper.find('input.locality').val(ui.item.city);
           const dropdown = $("select.administrative-area");
           const text = ui.item.province;
           $("select.administrative-area option").filter(function() {
             return $(this).text() == text;
           }).prop("selected", true);
           form_wrapper.find('input.postal-code').val(ui.item.zip_code);

         };
       });
    }
  };
}(jQuery, Drupal,once));
